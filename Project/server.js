const express=require("express");
const cors=require('cors');
const mongoose=require('mongoose');
require('dotenv').config();
require('./models/exercise.model');
require('./models/user.model');
const app = express();
const port =process.env.PORT||5000;
const exerciseRouter = require('./routes/exercises');
 const userRouter = require('./routes/users');
app.use(cors());
app.use(express.json());
//the below code is for online mongodb connection 
// const uri = process.env.ATLAS_URI;
// mongoose.connect(uri,{useNewUrlParser:true,useCreateIndex:true,useUnifiedTopology: true}
//     );
//     const connection =mongoose.connection;
//     connection.once('open',()=>{
//     console.log("MongoDb database connection established successfully");
// })
mongoose.connect('mongodb://localhost:27017/EmployeeCB',{useNewUrlParser:true},(err)=>{
    if (!err) {console.log('mongodb connection suucced')}
    else {console.log('error in DB connection:'+ err)}
});
app.use('/exercises',exerciseRouter);
app.use('/users',userRouter);
app.listen(port,()=>{
    console.log(`server is running on port:${port}`);
});   