const moongose=require('mongoose');
const Schema=moongose.Schema;
const userSchema=new Schema({
    leavetype :{type:String ,required:true},
    fromdate :{type:Date, required:true},
    todate: {type:Date,required:true},
    remarks:{type:String,required:true},
    status:{type:String},
},{
        timestamps:true,

    
});
const User=moongose.model('User',userSchema);
module.exports =User;

