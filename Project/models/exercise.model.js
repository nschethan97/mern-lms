const mongoose =require('mongoose');
const Schema = mongoose.Schema;
const exerciseSchema = new Schema({
    firstname : { type : String,  required:true, unique:true},
    lastname : {type:String, required:true},
    password : '',
    email:{type:String,unique:true, required:true},
    roll_type:'',
},{ 
    timestamps:true,
}); 
const Exercise= mongoose.model('Exercise',exerciseSchema);
module.exports = Exercise;