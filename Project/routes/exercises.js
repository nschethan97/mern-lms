const router = require('express').Router();
let Exercise = require('../models/exercise.model');

router.route('/').get((req,res)=>{
    Exercise.find()
    .then(exercise=>res.json(exercise))
    .catch(err=>res.status(400).json('Error:'+err));
});
router.route('/add').post((req,res)=>{
    const firstname =req.body.firstname;
    const lastname =req.body.lastname;
    const password =(req.body.password);
    const email=req.body.email;
    // const role_type=req.body.role_type;

    const newExercise =new Exercise({firstname,
    lastname,
password,
email,
});
    newExercise.save()
    .then(()=>res.json('Exercise added!'))
    .catch(err=>res.status(400).json('Error:'+err));
});
router.route('/:id').get((req,res)=>{
    Exercise.findById(req.params.id)
    .then(exercise=>res.json(exercise))
    .catch(err=>res.status(400).json('Error:'+err));
});
router.route('/:id').delete((req,res)=>{
    Exercise.findByIdAndDelete(req.params.id)
    .then(()=>res.json('Exercise deleted!'))
    .catch(err=>res.status(400).json('Error:'+err));
});
router.route('/update/:id').put((req,res)=>{
    Exercise.findById(req.params.id)
    .then(exercise=>{
        exercise.firstname=req.body.firstname;
        exercise.lastname=req.body.lastname;
        exercise.password=(req.body.password);
        exercise.email=req.body.email;

        exercise.save()
        .then(()=>res.json('Details updated'))
        .catch(err=>res.status(400).json('Error:'+err));
    })
    .catch(err=>res.status(400).json('Error'+err));
});
router.route('/login').post((req,res)=>{
    const password =req.body.password;
    const email=req.body.email;
    Exercise.findOne({email:email,password:password},function(err,user){
        if(err){
            console.log(err);
            return res.status(500).send('error');
        }
        if(!user){
            return res.status(204).send('loginfailed');
        }
        return res.status(200).send('login successful');
    })
   
})
module.exports=router; 