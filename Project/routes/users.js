const router = require('express').Router();
let User = require('../models/user.model');
router.route('/').get((req,res)=>{
    User.find()
    .then(users=>res.json(users))
    .catch(err=>res.status(400).json('Error:'+err));
});
router.route('/add').post((req,res)=>{
    const leavetype =req.body.leavetype;
    const fromdate =Date.parse(req.body.fromdate);
    const todate =Date.parse(req.body.todate);
    const remarks = req.body.remarks;

    const status= "default";
    const newUser=new User({
        leavetype,
         fromdate,
         todate,
         remarks,
         status,
});
    newUser.save()
    .then(()=>res.json('Record added!'))
    .catch(err=>res.status(400).json('Error:'+err));
});
router.route('/:id').get((req,res)=>{
    User.findById(req.params.id)
    .then(exercise=>res.json(exercise))
    .catch(err=>res.status(400).json('Error:'+err));
});
router.route('/:id').delete((req,res)=>{
    User.findByIdAndDelete(req.params.id)
    .then(()=>res.json('Record deleted!'))
    .catch(err=>res.status(400).json('Error:'+err));
});
router.route('/update/:id').put((req,res)=>{
    User.findById(req.params.id)
    .then(exercise=>{
        exercise.leavetype=req.body.leavetype;
        exercise.fromdate=Date(req.body.fromdate);
        exercise.todate=Date(req.body.todate);
        exercise.remarks=req.body.remarks;
        exercise.status=req.body.status;
        exercise.save()
        .then(()=>res.json('Record updated'))
        .catch(err=>res.status(400).json('Error:'+err));
    })
    .catch(err=>res.status(400).json('Error'+err));
});
module.exports=router;